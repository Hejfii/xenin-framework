![logo](assets/images/xenin.png)

# The Xenin Framework
> A powerful framework developed for Garry’s Mod.

[Source Code](https://gitlab.com/sleeppyy/xenin-framework)
[Get Started](/installation.md)