# Icon Downloader <client>Client</client>
Asynchronous icon downloader.

# Methods

## DownloadIcon
```laux
XeninUI:DownloadIcon(pnl: Panel, tbl: table, pnlVar: string)
```
**Arguments**
1. Panel to store the icon.
2. Parameters
3. Icon identifier

---

## DrawIcon
```laux
XeninUI:DrawIcon(x: number, y: number: number, h: number, pnl: Panel, col: Color, loadCol: Color, var: string)
```
**Arguments**
1. The X coordinate.
2. The Y coordinate.
3. The width of the icon.
4. The height of the icon.
5. Panel that contains the icon.
6. Icon color.
7. Loading circle color (if icon isn't downloaded).
8. Icon identifier

---

## GetIcon <small><deprecated>Deprecated</deprecated></small>
```laux
XeninUI:GetIcon(id: string|IMaterial)
```
**Arguments**
1. Icon identifier

# Example
This example come from the Xenin Framework (`laux/xeninui/libs/icon_dl.laux`).
```laux
local test = {
  -- It checks the first image, if that fails to load then the second, then third, etc..
  -- First one is cusotm, using a cusotm URL n not using png.
  { id = "dLyjNp", url = "https://i.hizliresim.com", type = "jpg" },
  -- If there's no url specificed it'll use imgur, and if there's no type, it'll use png
  { id = "4aa0Ka4" }
}

-- Example frame
XeninUI.frame = vgui.Create("XeninUI.Frame")
XeninUI.frame:SetSize(800, 600)
XeninUI.frame:Center()
XeninUI.frame:MakePopup()

XeninUI.panel = XeninUI.frame:Add("DPanel")
XeninUI.panel:Dock(FILL)
XeninUI:DownloadIcon(XeninUI.panel, test)
XeninUI.panel.Paint = function(pnl, w, h)
  XeninUI:DrawIcon(8, 8, w - 16, h - 16, pnl)
end
```